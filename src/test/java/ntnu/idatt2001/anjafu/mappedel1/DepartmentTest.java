package ntnu.idatt2001.anjafu.mappedel1;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class DepartmentTest {
    Department d;
    Employee e;
    Patient p;

    @BeforeEach
    void initAll(){
        d = new Department("Test");
        e = new Employee("Ann","Fur","171001");
        p = new Patient("Lise","Tom","180101");
    }

    @Nested
    @DisplayName("Remove existing person")
    class RemoveExistingPerson{

        @Test
        @DisplayName("Remove existing employee")
        void removeExistingEmployee() {
            d.addEmployee(e);

            try {
                d.remove(e);
            }catch (RemoveException re){
            }finally {
                assertFalse(d.getEmployees().contains(e));
            }
        }

        @Test
        @DisplayName("Remove existing patient")
        void removeExistingPatient() {
            d.addPatient(p);

            try {
                d.remove(p);
            }catch (RemoveException re){
            }finally {
                assertFalse(d.getPatients().contains(p));
            }
        }
    }


    @Nested
    @DisplayName("Remove not existing person")
    class RemoveNotExistingPerson{

        @Test
        @DisplayName("Remove not existing employee")
        void removeNotExistingEmployee() {
            try {
                d.remove(e);
            }catch (RemoveException re){
                assertEquals(re.getMessage(),  e.getFullName()+" was not deleted, because they aren't registered.");
            }
        }

        @Test
        @DisplayName("Remove not existing patient")
        void removeNotExistingPatient() {
            try {
                d.remove(p);
            }catch (RemoveException re){
                assertEquals(re.getMessage(), p.getFullName()+" was not deleted, because they aren't registered.");
            }
        }
    }

}