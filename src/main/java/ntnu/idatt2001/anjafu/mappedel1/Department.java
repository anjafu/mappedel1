package ntnu.idatt2001.anjafu.mappedel1;

import java.util.HashSet;
import java.util.Objects;

/**
 * Class for one department in a hospital.
 * A department has a name, employees and patients.
 */
public class Department {
    private String departmentName;
    /*using hashset for the lists, because hashset cannot contain multiples, hence i need to check less
      in certain methods*/
    private HashSet<Employee> employees;
    private HashSet<Patient> patients;

    /**
     * Constructor that creates a new department
     * @param departmentName - the name of the department
     */
    public Department(String departmentName) {
        //checks whether the name is acceptable or not (here: only acceptable if it isnt blank)
        if(departmentName.isBlank()){
            throw new IllegalArgumentException("The department must have a name.");
        }
        this.departmentName = departmentName;
        this.employees = new HashSet<Employee>();
        this.patients = new HashSet<Patient>();
    }

    /**
     * method to get the name of the department
     * @return the name of the department
     */
    public String getDepartmentName() {
        return departmentName;
    }

    /**
     * method to change the name of the department
     * @param departmentName - the new name of the department
     */
    public void setDepartmentName(String departmentName) { this.departmentName = departmentName;
    }

    /**
     * method to get all of the employees belonging to the department
     * @return a list of all employees
     */
    public HashSet<Employee> getEmployees(){
        return employees;
    }

    /**
     * method to get all of the patients belonging to the department
     * @return a list of all patients
     */
    public HashSet<Patient> getPatients(){
        return patients;
    }

    /**
     * Method to add a new employee to the department
     *
     * Changes from original class diagram in the assignment: made the method return a boolean
     * rather than nothing, then it's easier to check whether it actually worked or not
     * @param e - the new employee
     * @return true if the employee was not registered already and therefore was added,
     * false if the employee was already registered or the employee was a null object
     */
    public boolean addEmployee(Employee e){
        //first checks if the employee object is actually acceptable to add
        if(e == null){
            return false;
        }
        //Don't have to check if it's already registered, since HashSet cannot contain duplicates
        return employees.add(e);
    }

    /**
     * method to add a new patient to the department
     *
     * Changes from original class diagram in the assignment: made the method return a boolean
     * rather than nothing, then it's easier to check whether it actually worked or not
     * @param p - the new patient
     * @return true if the patient was not registered already and therefore was added,
     * false if the patient was already registered or the employee was a null object
     */
    public boolean addPatient(Patient p){
        //first checks if the patient object is actually acceptable to add
        if(p == null){
            return false;
        }
        //Don't have to check if it's already registered, since HashSet cannot contain duplicates
        return patients.add(p);
    }

    /**
     * method to remove a person (patient or employee) from the department
     * @param p - the patient or employee that shall be removed
     * @throws RemoveException - if the employee/patient was not already registered
     */
    public void remove(Person p) throws RemoveException{
        boolean worked = false;

        if(p instanceof Patient){
            //the remove method to hashset returns true if the person p was registered
            worked = patients.remove(p);
        }else if(p instanceof Employee){
            worked = employees.remove(p);
        }

        //if the remove method does not return true, then the person p was not registered
        if(!worked){
            throw new RemoveException(p.getFullName()+" was not deleted, because they aren't registered.");
        }
    }

    /**
     * method that returns all the information belonging to a given department
     * @return all the contents to the department (name, employees and patients)
     */
    @Override
    public String toString() {
        return "Department: " + departmentName +
                ".\nEmployees:\n" + getEmployees() +
                "\nPatients:\n" + getPatients();
    }

    /**
     * method that checks whether or not two department objects are the same. They are the same if either:
     * 1. both the department name and employees are the same (patients can change all the time)
     * 2. both the objects are the same
     * @param o - the other object you want to compare with
     * @return true if the department objects are the same or false if they are not
     */
    @Override
    public boolean equals(Object o) {
        //checks if both of the objects point to the same object (and therefore are the same)
        if (this == o) return true;
        //checks if the object o is empty/null or a different type than department
        if (!(o instanceof Department)) return false;
        //know now that the object is of the department type
        Department d = (Department) o;
        //checks whether the name and employees of both the departments are the same
        return (this.departmentName).equalsIgnoreCase(d.getDepartmentName()) &&
                (this.employees).equals(d.employees);
    }

    /**
     * method that creates a unique id for a department object
     * @return the unique id for a department
     */
    @Override
    public int hashCode() {
        return Objects.hash(departmentName);
    }
}
