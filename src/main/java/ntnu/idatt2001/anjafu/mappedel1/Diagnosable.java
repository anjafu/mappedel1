package ntnu.idatt2001.anjafu.mappedel1;

/**
 * interface that is implemented by other objects that are diagnosable
 */
public interface Diagnosable {
    /**
     * method that sets the diagnosis (this method must be implemented by all classes
     * that implements this interface)
     * @param diagnosis - the new diagnosis
     */
    public void setDiagnosis(String diagnosis);
}
