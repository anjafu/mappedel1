package ntnu.idatt2001.anjafu.mappedel1;

/**
 * class that represents a nurse
 * a nurse is a type of employee at a hospital
 */
public class Nurse extends Employee{

    /**
     * constructor that creates a new nurse
     * @param firstName - first name to the nurse
     * @param lastName - last name to the nurse
     * @param socialSecurityNumber - social security number to the nurse
     */
    public Nurse(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * method that returns all the information belonging to a given nurse
     * @return all the contents to the nurse (full name and social security number)
     */
    @Override
    public String toString() {
        return "Nurse: "+super.toString();
    }
}
