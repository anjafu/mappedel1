package ntnu.idatt2001.anjafu.mappedel1;

/**
 * class that represents a patient
 * a patient is a type of person that can be diagnosed
 */
public class Patient extends Person implements Diagnosable{
    private String diagnosis;

    /**
     * constructor that creates a new patient
     * @param firstName - first name to the patient
     * @param lastName - last name to the patient
     * @param socialSecurityNumber - social security number to the patient
     */
    protected Patient(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * method that returns the diagnosis of the patient
     * @return the diagnosis of the given patient
     */
    protected String getDiagnosis() {
        return diagnosis;
    }

    /**
     * method that sets the diagnosis of a given patient
     * @param diagnosis - the new diagnosis the patient shall have
     */
    @Override
    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    /**
     * method that returns all the information belonging to a given patient
     * @return all the contents to the patient (full name and social security number)
     */
    @Override
    public String toString() {
        return "Patient: " + super.toString() + ", diagnosis: "+diagnosis;
    }
}
