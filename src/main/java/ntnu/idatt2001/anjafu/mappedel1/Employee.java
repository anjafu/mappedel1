package ntnu.idatt2001.anjafu.mappedel1;

/**
 * class that represents an employee
 * an employee is a type of person
 */
public class Employee extends Person{

    /**
     * constructor that creates a new employee
     * @param firstName - first name to the employee
     * @param lastName - last name to the employee
     * @param socialSecurityNumber - social security number to the employee
     */
    public Employee(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * method that returns all the information belonging to a given employee
     * @return all the contents to the employee (full name and social security number)
     */
    @Override
    public String toString() {
        return "Employee: " + super.toString();
    }
}
