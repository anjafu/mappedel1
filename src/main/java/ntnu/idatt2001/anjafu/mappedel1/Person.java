package ntnu.idatt2001.anjafu.mappedel1;

import java.util.Objects;

/**
 * abstract class that represents one person
 * A person has a name (first and last) and a social security number
 */
public abstract class Person {
    private String firstName;
    private String lastName;
    private String socialSecurityNumber;

    /**
     * constructor that creates a new person
     * @param firstName - the persons first name
     * @param lastName - the persons last name
     * @param socialSecurityNumber - the persons social security number
     */
    public Person(String firstName, String lastName, String socialSecurityNumber) {
        /*only checking name (even though SecurityNumber is also necessary) because HospitalTestData
        does not fill out socialSecurityNumber*/
        if(firstName.isBlank() || lastName.isBlank()){
            throw new IllegalArgumentException("A person must have a name.");
        }
        this.firstName = firstName;
        this.lastName = lastName;
        this.socialSecurityNumber = socialSecurityNumber;
    }

    /**
     * method to get the first name of a given person
     * @return the first name of a person
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * method to change the first name of a given person
     * @param firstName - the new first name
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * method to get the last name of a given person
     * @return the last name of the person
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * method to change the last name of a given person
     * @param lastName - the new last name
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * method to get the social security number of a given person
     * @return the social security number of a given person
     */
    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    /**
     * method to change the social security number of a given person
     * @param socialSecurityNumber - the new social security number
     */
    public void setSocialSecurityNumber(String socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }

    /**
     * method to get the full name of a person
     * @return the first and last name of a given person
     */
    public String getFullName() {
        return firstName+" "+lastName;
    }

    /**
     * method that returns all the information belonging to a given person
     * @return all the contents to the person (first name, last name, social security number)
     */
    @Override
    public String toString() {
        return  "First name: " + firstName +
                ", last name: " + lastName +
                ", social security number: " + socialSecurityNumber;
    }

    /**
     * This is an added method to this assignment: added it because it was necessary to state
     * what made two person objects the same so that the hashset methods could work correctly
     * (otherwise hashset only checked if both objects referenced to the same object)
     *
     * method that checks whether or not two person objects are the same. They are the same if either:
     * 1. both the full name and social security number are the same
     * 2. both the objects are the same
     * @param o - the other object you want to compare with
     * @return true if the person objects are the same or false if they are not
     */
    @Override
    public boolean equals(Object o) {
        //checks if both of the objects point to the same object (and therefore are the same)
        if (this == o) return true;
        //checks if the object o is empty/null or a different type than person
        if (o == null || getClass() != o.getClass()) return false;
        //know now that the object is of the person type
        Person person = (Person) o;
        //checks whether the full name and social security number of both the persons are the same
        return Objects.equals(firstName, person.firstName) &&
                Objects.equals(lastName, person.lastName) &&
                Objects.equals(socialSecurityNumber, person.socialSecurityNumber);
    }

    /**
     * method that creates a unique id for a person object
     * @return the unique id for a person
     */
    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, socialSecurityNumber);
    }
}
