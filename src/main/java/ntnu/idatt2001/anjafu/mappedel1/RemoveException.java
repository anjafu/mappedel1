package ntnu.idatt2001.anjafu.mappedel1;

/**
 * exception class that represents the exception if one tries to remove
 * an object that is not already registered
 */
public class RemoveException extends Exception{
    final static long serialVersionUID = 1L;

    /**
     * constructor that creates a new remove exception
     * @param message - the fail message belonging to the remove exception
     */
    public RemoveException(String message) {
        super(message);
    }
}
