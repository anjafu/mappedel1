package ntnu.idatt2001.anjafu.mappedel1;

/**
 * class that represents a general practitioner
 * a general practitioner is a type of doctor at a hospital
 */
public class GeneralPractitioner extends Doctor{

    /**
     * constructor that creates a new general practitioner
     * @param firstName - first name to the general practitioner
     * @param lastName - last name to the general practitioner
     * @param socialSecurityNumber - social security number to the general practitioner
     */
    public GeneralPractitioner(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * method that sets the diagnosis of a specific patient
     * @param patient - the patient whose diagnosis is to be set
     * @param diagnosis - the diagnosis the patient is getting
     */
    @Override
    public void setDiagnosis(Patient patient, String diagnosis) {
        patient.setDiagnosis(diagnosis);
    }
}
