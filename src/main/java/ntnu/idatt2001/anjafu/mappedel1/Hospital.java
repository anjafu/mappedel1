package ntnu.idatt2001.anjafu.mappedel1;

import java.util.HashSet;

/**
 * Class for one hospital
 * A hospital has a name and a number of departments
 */
public class Hospital {
    private final String hospitalName;
    /*using hashset for the lists, because hashset cannot contain multiples, hence i need to check less
      in certain methods*/
    private HashSet<Department> departments;

    /**
     * constructor that creates a new hospital
     * @param hospitalName - the name of the hospital
     */
    public Hospital(String hospitalName) {
        if(hospitalName.isBlank()){
            throw new IllegalArgumentException("The department must have a name.");
        }
        this.hospitalName = hospitalName;
        departments = new HashSet<>();
    }

    /**
     * method to get the name of the given hospital
     * @return the name of the hospital
     */
    public String getHospitalName() {
        return hospitalName;
    }

    /**
     * method to get all of the departments belonging to a given hospital
     * @return a list of all the departments to a given hospital
     */
    public HashSet<Department> getDepartments() {
        return departments;
    }

    /**
     * method to add a new department to the given hospital
     * @param d - the new department
     * @return true if it was registered (the department was not registered already),
     *         false if the department object was null or it was already registered
     */
    public boolean addDepartment(Department d){
        //first checks if the department object is actually acceptable to add
        if(d == null){
            return false;
        }
        //Don't have to check if it's already registered, since HashSet cannot contain duplicates
        return departments.add(d);
    }

    /**
     * method that returns all the information belonging to a given hospital
     * @return all the contents to the hosptial (name and departments)
     */
    @Override
    public String toString() {
        return "Hospital: " + hospitalName +
                ", departments: " + departments;
    }
}
