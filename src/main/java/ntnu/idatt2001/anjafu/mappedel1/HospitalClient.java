package ntnu.idatt2001.anjafu.mappedel1;

public class HospitalClient {
    public static void main(String[] args) {
        Hospital test = new Hospital("Test");
        HospitalTestData.fillRegisterWithTestData(test);

        for (Department d : test.getDepartments()) {
            try {
                if ((d.getDepartmentName()).equalsIgnoreCase("akutten")) {
                    /*know that the employee "odd even" is already registered in the department "akutten",
                     so removing him should not throw an exception*/
                    Employee e = new Employee("Odd Even", "Primtallet", "");
                    d.remove(e);
                    if(!(d.getEmployees().contains(e))){
                        System.out.println("Odd Even Primtallet was successfully removed");
                    }

                    /*know that the patient "Anja" is not registered in the department "akutten",
                      so removing her should throw an exception*/
                    d.remove(new Patient("Anja", "Fur", "1234"));
                }
            }catch (RemoveException e){
                /*the exception message includes the name of the person who was not removed, hence
                  it is a good thing to show the client*/
                System.out.println(e.getMessage());
            }
        }
    }
}
