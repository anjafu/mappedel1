package ntnu.idatt2001.anjafu.mappedel1;

/**
 * class that represents a doctor
 * a surgeon is a type of doctor at a hospital
 */
public class Surgeon extends Doctor{

    /**
     * constructor that creates a new surgeon
     * @param firstName - first name to the surgeon
     * @param lastName - last name to the surgeon
     * @param socialSecurityNumber - social security number to the surgeon
     */
    public Surgeon(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * method that sets the diagnosis of a specific patient
     * @param patient - the patient whose diagnosis is to be set
     * @param diagnosis - the diagnosis the patient is getting
     */
    @Override
    public void setDiagnosis(Patient patient, String diagnosis) {
        patient.setDiagnosis(diagnosis);
    }
}
