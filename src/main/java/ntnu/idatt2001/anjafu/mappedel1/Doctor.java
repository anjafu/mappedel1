package ntnu.idatt2001.anjafu.mappedel1;

/**
 * abstract class that represents a doctor
 * a doctor is a type of employee at a hospital
 */
public abstract class Doctor extends Employee{

    /**
     * constructor that creates a new doctor (since this is an abstract class, one cannot create
     * a doctor object, but this method is used by the subclasses to the doctor class)
     * @param firstName - first name to the doctor
     * @param lastName - last name to the doctor
     * @param socialSecurityNumber - social security number to the doctor
     */
    public Doctor(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * abstract method that sets the diagnosis of a specific patient (this method is to be
     * implemented by the subclasses of the doctor class)
     * @param patient - the patient whose diagnosis is to be set
     * @param diagnosis - the diagnosis the patient is getting
     */
    public abstract void setDiagnosis(Patient patient, String diagnosis);
}
